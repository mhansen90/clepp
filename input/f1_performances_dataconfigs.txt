Data Config	Data Type	Affinity Propagation	DBSCAN	Hierarchical Clustering	MCODE	Markov Clustering	Self Organizing Maps	Spectral Clustering	Transitivity Clustering	clusterONE	clusterdp	fanny	k-Means	k-Medoids (PAM)	Average F1-Score	Median F1-Score
astral	Protein Sequence Similarity	0.267	0.287	0.747	0.592	0.596	--	0.78	0.732	0.771	0.726	0.494	--	0.657	0.604	0.657
astral_40	Protein Structure Similarity	0.837	0.179	0.849	0.755	0.645	--	0.224	0.84	0.809	0.852	0.71	--	0.872	0.688	0.809
astral_40_seq_blast_100	Protein Sequence Similarity	0.544	0.261	0.714	0.463	0.649	--	0.457	0.714	0.634	0.664	0.48	--	0.576	0.56	0.576
boneMarrowGeneExpr_spearmanDist	Gene Expression	0.948	0.933	0.933	0.917	0.783	0.831	0.716	0.933	0.847	0.923	0.921	0.921	0.9	0.885	0.921
chang_pathbased	Synthetic	0.501	0.947	0.736	0.843	0.65	0.652	0.867	0.73	0.501	0.735	0.711	0.696	0.691	0.712	0.711
chang_spiral	Synthetic	0.5	1.0	0.422	0.484	0.5	0.408	0.93	0.5	0.5	1.0	0.5	0.463	0.418	0.587	0.5
coli_find	Word Sense Disambiguation	0.256	0.221	0.236	0.23	0.221	--	0.217	0.235	0.221	0.235	0.254	--	0.244	0.234	0.235
coli_need	Word Sense Disambiguation	0.55	0.55	0.546	0.55	0.55	--	0.493	0.55	0.55	0.547	0.551	--	0.546	0.544	0.55
coli_state	Word Sense Disambiguation	0.485	0.53	0.531	0.526	0.53	--	0.516	0.53	0.53	0.546	0.53	--	0.507	0.524	0.53
coli_time	Word Sense Disambiguation	0.349	0.403	0.402	0.382	0.403	--	0.403	0.403	0.403	0.404	0.404	--	0.387	0.395	0.403
DS1	Social Network	0.715	0.668	1.0	0.668	1.0	--	0.69	1.0	0.884	0.971	1.0	--	0.941	0.867	0.941
fraenti_s3	Synthetic	0.547	0.63	0.79	0.347	0.125	0.375	0.85	0.835	0.125	0.855	0.84	0.844	0.844	0.616	0.79
fu_flame	Synthetic	0.689	0.987	0.904	0.598	0.689	0.845	0.889	0.866	0.689	1.0	0.869	0.841	0.853	0.825	0.853
gionis_aggregation	Synthetic	0.485	0.864	0.967	0.663	0.345	0.544	0.943	0.975	0.345	1.0	0.892	0.893	0.858	0.752	0.864
ppi_mips	Protein-Protein-Interaction	0.17	0.89	0.88	0.805	0.821	--	0.535	0.881	0.87	0.311	0.888	--	0.822	0.716	0.822
sfld_1	Protein Sequence Similarity	0.91	0.68	0.986	0.931	0.923	--	0.656	0.986	0.946	0.975	0.767	--	0.912	0.879	0.923
synthetic_cassini250	Synthetic	0.524	1.0	1.0	0.992	0.524	0.778	1.0	0.885	0.524	1.0	0.957	0.78	0.95	0.84	0.95
synthetic_cuboid250	Synthetic	0.657	1.0	1.0	0.888	0.415	0.651	0.709	0.961	0.415	1.0	1.0	1.0	1.0	0.823	0.961
synthetic_spirals250	Synthetic	0.667	1.0	0.512	0.637	0.667	0.504	0.698	0.667	0.667	1.0	0.667	0.502	0.504	0.669	0.667
tcga	Gene Expression	0.389	0.944	0.998	0.894	0.678	--	0.5	0.986	0.678	0.921	0.914	--	0.9	0.8	0.9
twonorm_100d	Synthetic	0.342	0.667	0.787	0.667	0.667	0.666	0.965	0.667	0.788	0.666	0.899	0.965	0.66	0.724	0.667
twonorm_50d	Synthetic	0.5	0.667	0.897	0.717	0.667	0.98	0.97	0.827	0.925	0.825	0.975	0.975	0.859	0.83	0.859
veenman_r15	Synthetic	0.997	0.989	0.994	0.969	0.125	0.98	0.952	0.997	0.125	0.997	0.989	0.974	0.997	0.853	0.989
zahn_compound	Synthetic	0.581	0.886	0.852	0.825	0.381	0.611	0.8	0.848	0.381	0.868	0.811	0.769	0.769	0.722	0.8
